function toggle(element) {
	var container = document.getElementById("menu");
	var collection = container.getElementsByTagName("a");
	for (var i = 0; i < collection.length; i++) {
		var item = collection[i];
		if (item.id == element.id) {
			removeAllInteractions(); //HACK
			switch (item.id) {
				case "navigate":
					addToLog("Выбран режим навигации");
					break;
				case "point":
					changeDrawInteraction("Point");
					addToLog("Выбран режим рисования: Точка");
					break;
				case "line":
					changeDrawInteraction("LineString");
					addToLog("Выбран режим рисования: Линия");
					break;
				case "polygon":
					changeDrawInteraction("Polygon");
					addToLog("Выбран режим рисования: Полигон");
					break;
				case "drag":
					addDragInteraction();
					addToLog("Выбран режим перемещения объекта");
					break;
				case "delete":
					addDisposeInteraction();
					addToLog("Выбран режим удаления объекта");
					break;
				case "modify":
					addModifyInteraction();
					addToLog("Выбран режим редактирования объекта");
					break;
				case "select":
					addSelectInteraction();
					addToLog("Выбран режим информации об объекте");
					break;
				case "length":
					addMeasureInteraction("LineString");
					addToLog("Выбран режим измерения длины");
					break;
				case "area":
					addMeasureInteraction("Polygon");
					addToLog("Выбран режим измерения площади");
					break;
			}
			highlight(item, true);
		} else {
			highlight(item, false);
		}
	}
}

function removeAllInteractions() {
	removeDrawInteraction();
	removeDragInteraction();
	removeDisposeInteraction();
	removeModifyInteraction();
	removeSelectInteraction();
	removeMeasureInteraction();
}

function highlight(element, mode) {
	if (element) {
		element.style.background = mode ? "#B6D1BA" : "#DDEDE4";
	}
}

function showLog() {
	$("#dialog_log").dialog({width: 640, height: 480});
}

function addToLog(text) {
	var today = new Date();
	var day = today.getDate();
	var month = today.getMonth() + 1; //January is 0!
	var year = today.getFullYear();
	var hour = today.getHours();
	var minute = today.getMinutes();
	today = day + "." + month + "." + year + ":" + hour + "." + minute;
	$("#dialog_log").append("<p>" + today + ": " + text + "</p>");
}

function showWMS() {
	$("#dialog_ows").dialog({width: 640, height: 480});
}