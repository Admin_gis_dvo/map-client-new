/**
 * Add getLayer method for Map object. Requires ol3-layerswitcher.js
 */
if (ol.Map.prototype.getLayer === undefined) {
	ol.Map.prototype.getLayer = function (id) {
		var layer;
		ol.control.LayerSwitcher.forEachRecursive(this, function(lyr, idx, a) {
			if (id == lyr.get("id")) {
				layer = lyr;
			}
		});
		return layer;
	}
}

/**
 * Define a namespace for the application.
 */
window.app = {};
var app = window.app;
addToLog("Создано окно");

/**
 * @constructor
 * @extends {ol.interaction.Pointer}
 */
app.Drag = function() {
	ol.interaction.Pointer.call(this, {
		handleDownEvent: app.Drag.prototype.handleDownEvent,
		handleDragEvent: app.Drag.prototype.handleDragEvent,
		handleMoveEvent: app.Drag.prototype.handleMoveEvent,
		handleUpEvent: app.Drag.prototype.handleUpEvent
	});

	/**
	* @type {ol.Pixel}
	* @private
	*/
	this.coordinate_ = null;

	/**
	* @type {string|undefined}
	* @private
	*/
	this.cursor_ = "pointer";

	/**
	* @type {ol.Feature}
	* @private
	*/
	this.feature_ = null;

	/**
	* @type {string|undefined}
	* @private
	*/
	this.previousCursor_ = undefined;
};
ol.inherits(app.Drag, ol.interaction.Pointer);

/**
 * @param {ol.MapBrowserEvent} evt Map browser event.
 * @return {boolean} `true` to start the drag sequence.
 */
app.Drag.prototype.handleDownEvent = function(evt) {
	var map = evt.map;
	var feature = map.forEachFeatureAtPixel(evt.pixel,
		function(feature, layer) {
			return feature;
		}
	);
	if (feature) {
		this.coordinate_ = evt.coordinate;
		this.feature_ = feature;
	}
	return !!feature;
};

/**
 * @param {ol.MapBrowserEvent} evt Map browser event.
 */
app.Drag.prototype.handleDragEvent = function(evt) {
	var map = evt.map;
	var feature = map.forEachFeatureAtPixel(evt.pixel,
		function(feature, layer) {
			return feature;
		}
	);
	var deltaX = evt.coordinate[0] - this.coordinate_[0];
	var deltaY = evt.coordinate[1] - this.coordinate_[1];
	var geometry = /** @type {ol.geom.SimpleGeometry} */ (this.feature_.getGeometry());
	geometry.translate(deltaX, deltaY);
	this.coordinate_[0] = evt.coordinate[0];
	this.coordinate_[1] = evt.coordinate[1];
};

/**
 * @param {ol.MapBrowserEvent} evt Event.
 */
app.Drag.prototype.handleMoveEvent = function(evt) {
	if (this.cursor_) {
		var map = evt.map;
		var feature = map.forEachFeatureAtPixel(evt.pixel,
			function(feature, layer) {
				return feature;
			}
		);
		var element = evt.map.getTargetElement();
		if (feature) {
			if (element.style.cursor != this.cursor_) {
				this.previousCursor_ = element.style.cursor;
				element.style.cursor = this.cursor_;
			}
		} else if (this.previousCursor_ !== undefined) {
			element.style.cursor = this.previousCursor_;
			this.previousCursor_ = undefined;
		}
	}
};

/**
 * @param {ol.MapBrowserEvent} evt Map browser event.
 * @return {boolean} `false` to stop the drag sequence.
 */
app.Drag.prototype.handleUpEvent = function(evt) {
	this.coordinate_ = null;
	this.feature_ = null;
	return false;
};

/**
 * Elements that make up the popup.
 */
var container = document.getElementById("popup");
var content = document.getElementById("popup-content");
var closer = document.getElementById("popup-closer");
addToLog("Создан контейнер для всплывающего окна");

/**
 * Add a click handler to hide the popup.
 * @return {boolean} Don't follow the href.
 */
closer.onclick = function() {
	overlay.setPosition(undefined);
	closer.blur();
	return false;
};

/**
 * Create an overlay to anchor the popup to the map.
 */
var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
	element: container,
	autoPan: true,
	autoPanAnimation: {
		duration: 250
	}
}));

/**
 * Create map
 */
var map = new ol.Map({
	layers: [
		new ol.layer.Group({
			"title": "Подложки",
			layers: [
				new ol.layer.Tile({
					title: "Карта",
					type: "base",
					visible: true,
					source: new ol.source.OSM()
				}),
				new ol.layer.Tile({
					title: "Спутник",
					type: "base",
					visible: false,
					source: new ol.source.MapQuest({layer: "sat"})
				})
			]
		})
	],
	overlays: [overlay],
	target: "map",
	controls: ol.control.defaults().extend([
		new ol.control.ScaleLine({
			units: "metric"
		}),
		new ol.control.LayerSwitcher({
			tipLabel: "Показать слои" // Optional label for button
		})
	]),
	view: new ol.View({
		center: [0, 0],
		zoom: 2
	})
});
addToLog("Создана карта и слои с подложками");

var hoverPoints = new ol.style.Style({
	image: new ol.style.Circle({
		fill: new ol.style.Fill({
			color: "#F29DC1"
		}),
		stroke: new ol.style.Stroke({
			color: "#E83F85"
		}),
		radius: 6
	})
});
var hoverLines = new ol.style.Style({
	fill: new ol.style.Fill({
		color: "#F29DC1"
	}),
	stroke: new ol.style.Stroke({
		color: "#E83F85",
		width: 2
	})
});

var selectPoints = new ol.style.Style({
	image: new ol.style.Circle({
		fill: new ol.style.Fill({
			color: "#AFB7ED"
		}),
		stroke: new ol.style.Stroke({
			color: "#7483F2"
		}),
		radius: 6
	})
});
var selectLines = new ol.style.Style({
	fill: new ol.style.Fill({
		color: "#AFB7ED"
	}),
	stroke: new ol.style.Stroke({
		color: "#7483F2",
		width: 2
	})
});

var hover = new ol.interaction.Select({
	condition: ol.events.condition.pointerMove,
	style: [hoverLines, hoverPoints]
});
function activateHover() {
	hover.on("select", function(e) {
		var count = e.selected.length;
		if (count > 0) {
		} else {
		}
	});
}
function deactivateHover() {
	hover.un("select");
}
activateHover();

/**
 * The features are not added to a regular vector layer/source,
 * but to a feature overlay which holds a collection of features.
 * This collection is passed to the modify and also the draw
 * interaction, so that both can add or modify features.
 */
var featureOverlay = new ol.FeatureOverlay({
	style: new ol.style.Style({
		fill: new ol.style.Fill({
			color: "rgba(255, 255, 255, 0.7)"
		}),
		stroke: new ol.style.Stroke({
			color: "#FFCC33",
			width: 2
		}),
		image: new ol.style.Circle({
			radius: 7,
			fill: new ol.style.Fill({
				color: "#FFCC33"
			})
		})
	})
});
featureOverlay.setMap(map);
addToLog("Создан слой для рисования");

var modifiers = new ol.interaction.Select({
	style: [selectLines, selectPoints]
}); // what to modify
var modify = new ol.interaction.Modify({
	features: modifiers.getFeatures(),
	// The SHIFT key must be pressed to delete vertices, so
	// that new vertices can be drawn at the same position
	// of existing vertices
	deleteCondition: function(event) {
	return ol.events.condition.shiftKeyOnly(event) &&
		ol.events.condition.singleClick(event);
	}
});
function addModifyInteraction() {
	map.addInteraction(hover);
	map.addInteraction(modifiers);
	map.addInteraction(modify);
}
function removeModifyInteraction() {
	map.removeInteraction(hover);
	map.removeInteraction(modifiers);
	map.removeInteraction(modify);
}

var drag = new app.Drag();
function addDragInteraction() {
	map.addInteraction(drag);
	map.addInteraction(hover);
}
function removeDragInteraction() {
	map.removeInteraction(drag);
	map.removeInteraction(hover);
}

var draw; // global so we can remove it later
function addDrawInteraction(featureType) {
	if (featureType != "None") {
		draw = new ol.interaction.Draw({
			features: featureOverlay.getFeatures(),
			type: /** @type {ol.geom.GeometryType} */ (featureType)
		});
		map.addInteraction(draw);
	}
}
function removeDrawInteraction() {
	map.removeInteraction(draw);
}
function changeDrawInteraction(featureType) {
	removeDrawInteraction();
	addDrawInteraction(featureType);
}

function getCenterOfExtent(extent) {
	var x = extent[0] + (extent[2] - extent[0]) / 2;
	var y = extent[1] + (extent[3] - extent[1]) / 2;
	return [x, y];
}

var select = new ol.interaction.Select({
	condition: ol.events.condition.click,
	style: [selectLines, selectPoints]
});
select.on("select", function(e) {
	var count = e.selected.length;
	if (count > 0) { // show popup
		var feature = e.target.getFeatures().item(0);
		var geometry = feature.getGeometry();
		var extent = geometry.getExtent();
		var coordinate = getCenterOfExtent(extent);
		var id = feature.get("id");
		var type = geometry.getType();
		var layout = geometry.getLayout();
		content.innerHTML =
			"<p>Вы выбрали объект:</p><code>" +
				"<div>" + "Тип: " + type + "</div>" +
				"<div>" + "Название: " + id + "</div>" +
				"<div>" + "Координаты: " + layout + "</div>" +
				"<div>" + "Центр: " + coordinate + "</div>" +
				"<div>" + "Экстент: " + extent + "</div>" +
			"</code>";
		overlay.setPosition(coordinate);
	} else { // hide popup
		overlay.setPosition(undefined);
		closer.blur();
	}
});
function addSelectInteraction() {
	map.addInteraction(select);
	map.addInteraction(hover);
}
function removeSelectInteraction() {
	map.removeInteraction(select);
	map.removeInteraction(hover);
}

var dispose = new ol.interaction.Select({
	conditions: [ol.events.condition.click]
});
dispose.on("select", function(e) {
	var count = e.selected.length;
	if (count > 0) { // delete feature
		var feature = e.target.getFeatures().item(0);
		featureOverlay.removeFeature(feature);
		dispose.getFeatures().clear();
		hover.getFeatures().clear();
		// HACK
		map.renderSync(); // forced redraw
	}
});
function addDisposeInteraction() {
	map.addInteraction(dispose);
	map.addInteraction(hover);
}
function removeDisposeInteraction() {
	map.removeInteraction(dispose);
	map.removeInteraction(hover);
}

/**
 * Currently drawn measure feature.
 * @type {ol.Feature}
 */
var sketch;

/**
 * The help tooltip element.
 * @type {Element}
 */
var helpTooltipElement;

/**
 * Overlay to show the help messages.
 * @type {ol.Overlay}
 */
var helpTooltip;

/**
 * The measure tooltip element.
 * @type {Element}
 */
var measureTooltipElement;

/**
 * Overlay to show the measurement.
 * @type {ol.Overlay}
 */
var measureTooltip;

/**
 * Message to show when the user is drawing a polygon.
 * @type {string}
 */
var continuePolygonMsg = "Нажмите чтобы продолжить рисовать полигон";

/**
 * Message to show when the user is drawing a line.
 * @type {string}
 */
var continueLineMsg = "Нажмите чтобы продолжить рисовать линию";

/**
 * Handle pointer move.
 * @param {ol.MapBrowserEvent} evt
 */
 var pointerMoveHandler = function(evt) {
	if (evt.dragging) {
		return;
	}
	/** @type {string} */
	var helpMsg = "Нажмите чтобы начать рисовать";
	/** @type {ol.Coordinate|undefined} */
	var tooltipCoord = evt.coordinate;
	if (sketch) {
		var output;
		var geom = (sketch.getGeometry());
		if (geom instanceof ol.geom.Polygon) {
			output = formatArea(/** @type {ol.geom.Polygon} */ (geom));
			helpMsg = continuePolygonMsg;
			tooltipCoord = geom.getInteriorPoint().getCoordinates();
		} else if (geom instanceof ol.geom.LineString) {
			output = formatLength( /** @type {ol.geom.LineString} */ (geom));
			helpMsg = continueLineMsg;
			tooltipCoord = geom.getLastCoordinate();
		}
		if (measureTooltipElement) {
			measureTooltipElement.innerHTML = output;
			measureTooltip.setPosition(tooltipCoord);
		}
	}
	if (helpTooltipElement) {
		helpTooltipElement.innerHTML = helpMsg;
		helpTooltip.setPosition(evt.coordinate);
	}
};

var measure;
var source = new ol.source.Vector();
function addMeasureInteraction(type) {
	/*
	 type: Polygon (area) or LineString (length)
	*/
	measure = new ol.interaction.Draw({
		source: source,
		type: /** @type {ol.geom.GeometryType} */ (type),
		style: new ol.style.Style({
			fill: new ol.style.Fill({
				color: "rgba(255, 255, 255, 0.2)"
			}),
			stroke: new ol.style.Stroke({
				color: "rgba(0, 0, 0, 0.5)",
				lineDash: [10, 10],
				width: 2
			}),
			image: new ol.style.Circle({
				radius: 5,
				stroke: new ol.style.Stroke({
					color: "rgba(0, 0, 0, 0.7)"
				}),
				fill: new ol.style.Fill({
				color: "rgba(255, 255, 255, 0.2)"
				})
			})
		})
	});
	map.addInteraction(measure);

	createMeasureTooltip();
	createHelpTooltip();

	measure.on("drawstart", function(evt) {
		// set sketch
		sketch = evt.feature;
	}, this);

	measure.on("drawend", function(evt) {
		measureTooltipElement.className = "tooltip tooltip-static";
		measureTooltip.setOffset([0, -7]);
		// unset sketch
		sketch = null;
		// unset tooltip so that a new one can be created
		createMeasureTooltip();
	}, this);
}
function removeMeasureInteraction() {
	removeHelpTooltip();
	removeMeasureTooltip();
	map.removeInteraction(measure);
}

/**
 * Creates a new help tooltip
 */
function createHelpTooltip() {
	removeHelpTooltip();
	helpTooltipElement = document.createElement("div");
	helpTooltipElement.className = "tooltip";
	helpTooltip = new ol.Overlay({
		element: helpTooltipElement,
		offset: [15, 0],
		positioning: "center-left"
	});
	map.addOverlay(helpTooltip);
}

/**
 * Removes an old help tooltip
 */
function removeHelpTooltip() {
	if (helpTooltipElement) {
		helpTooltipElement.parentNode.removeChild(helpTooltipElement);
	}
	helpTooltipElement = null;
}

/**
 * Creates a new measure tooltip
 */
function createMeasureTooltip() {
	removeMeasureTooltip();
	measureTooltipElement = document.createElement("div");
	measureTooltipElement.className = "tooltip tooltip-measure";
	measureTooltip = new ol.Overlay({
		element: measureTooltipElement,
		offset: [0, -15],
		positioning: "bottom-center"
	});
	map.addOverlay(measureTooltip);
}

/**
 * Removes an old measure tooltip
 */
function removeMeasureTooltip() {
	if (measureTooltipElement) {
		measureTooltipElement.parentNode.removeChild(measureTooltipElement);
	}
	measureTooltipElement = null;
}

/**
 * format length output
 * @param {ol.geom.LineString} line
 * @return {string}
 */
var formatLength = function(line) {
	var length = Math.round(line.getLength() * 100) / 100;
	var output;
	if (length > 100) {
		output = (Math.round(length / 1000 * 100) / 100) + " км";
	} else {
		output = (Math.round(length * 100) / 100) + " м";
	}
	return output;
};

/**
 * format length output
 * @param {ol.geom.Polygon} polygon
 * @return {string}
 */
var formatArea = function(polygon) {
	var area = polygon.getArea();
	var output;
	if (area > 10000) {
		output = (Math.round(area / 1000000 * 100) / 100) + " км<sup>2</sup>";
	} else {
		output = (Math.round(area * 100) / 100) + " м<sup>2</sup>";
	}
	return output;
};

map.on("pointermove", pointerMoveHandler);
addToLog("Установлен обработчик событий движения мыши");
toggle(document.getElementById("navigate"));